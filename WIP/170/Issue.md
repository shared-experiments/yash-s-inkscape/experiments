#### Introduciton
 After some time working in Inkscape I made some observations on how could be the workflow of bezier cuves improved.

#### Bezier tool

-  Show nodes you created not just first one so you can  edit as you go

![1._show_nodes](https://gitlab.com/inkscape/inbox/uploads/a5f5bcc601c1fe1260b037768c20b1d9/1._show_nodes.gif)

- Add new key modifier **ALT** :

 When you press and hold ALT pen tool behaves like Node tool This would be useful:
for changing length on one side of the handle.

![2._new_modifier](https://gitlab.com/inkscape/inbox/uploads/4ea0c65a78d0dc099e17d9cb2da6f504/2._new_modifier.gif)


- For deleting one side of the handle without shortcut SHIFT+L
You could just :

Option: click last node

Option: hold ALT double click handle control point 

Option: hold ALT+CTRL and click on the handle control point 

Option: hold ALT and click on the last node

![3._new_modifier_3_3](https://gitlab.com/inkscape/inbox/uploads/253af3f3ea88082950fed2c2b245ca2e/3._new_modifier_3_3.gif)


- Add a key modifier to move node after you put it down **ALT+ SPACE BAR**
(or shift+ spacebar)

![3._new_modifier_2_2](https://gitlab.com/inkscape/inbox/uploads/0118f5d9c4b0f0d12603a3698e3e2f99/3._new_modifier_2_2.gif)

- https://gitlab.com/https://gitlab.com/inkscape/inbox/issues/32 Fix bug with random corner points (it's very hard to record this bug it's very random ) but i reported this bug before


#### Node tool + modifiers
1. New keyboard modifiers:

**hold a bezier handle (left click) and then:**

**CTRL**
snaps to the angles

![3._node_1_1](https://gitlab.com/inkscape/inbox/uploads/857163e8b74887fcc4048306ccf3c319/3._node_1_1.gif)

**Shift**
brak handles apart

![3._node_3_3](https://gitlab.com/inkscape/inbox/uploads/b4a6cef587fb7682e1a23a3c822800a6/3._node_3_3.gif)

**Alt**
make them linked and with the same length

![3._node_2_2](https://gitlab.com/inkscape/inbox/uploads/e5a6d4c7aaca6326817409e6e916e32a/3._node_2_2.gif)

**SHIFT+SPACE**
Move node

![3._node_4_1_1](https://gitlab.com/inkscape/inbox/uploads/50e12ca680e44c872f3a445ab8aba9a0/3._node_4_1_1.gif)

**No shortcut**
make them linked but change the size of just one side (now defolut beheiveior)

![3._node_1_1](https://gitlab.com/inkscape/inbox/uploads/857163e8b74887fcc4048306ccf3c319/3._node_1_1.gif)

#### Node tool - Select and manipulet multiple  bezier hendels 

Let me select and manipulate multiple handles of different nodes at once.
Propos solution:

**SELECTING handle control point:**

- Click on a handle control point /or click and drag to select
- Hold Shift and click next one that you want to add to selection
- Hold Shift and click on a handle control point that is already in selection to remove that point form selection

**Manipulation behavior:**

You can select 3 modes which could be changed in UI top panel and Shortcut:
- 1 MODE: all linked 
All handles move together relatively linked to the position of a handle you are manipulating
click and drag on any a handle control point form selection  to move all of them

- 2 MODE: continue in the direction

- 3 MODE: Continue in reverse direction

![2019-02-28_16-10-45_1__1_](https://gitlab.com/inkscape/inbox/uploads/fdedcaaed1353c44b1c2383a11b9cc6f/2019-02-28_16-10-45_1__1_.gif)

#### Summary
All shorcuts are up to debate its more about this function to be added as a modifires for faster workflow for pro users.
Let me know what do you thiuink about ties changes and how hard it would be to implement.
