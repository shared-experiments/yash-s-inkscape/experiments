## Split View Mode

The new Split View Mode features a draggable separator that becomes visible as soon as the Split view mode has been activated. On one side of the separator, the canvas will look just like before, while on the other side, everything will be displayed in outline mode, and objects can more easily be grabbed with the mouse or edited with the node tool.

It can be moved on the canvas by grabbing either the separation line or the central handle. The sides can be switched by clicking on one of the little arrows on the handle.

Activate it with one of:

+ View → Split View Mode
+ Ctrl + 6
To deativate the mode, either deactivate the checkbox in the menu again, use the keyboard shortcut a second time, or drag the separator off the canvas.

Split Screen-smaller.gif

## X-Ray Mode

When the X-Ray Mode is active, a circular area that shows objects on the canvas in outline mode will follow the mouse pointer. This makes editing complex drawings with many objects layered on top of one another much easier, and is especially useful when used with the node tool.

Activate it with:

+ View → XRay Mode
+ Alt + 6
The size of the X-Ray circle can be adjusted in Edit → Preferences → Rendering → Rendering XRay radius.

To deactivate the mode, either deactivate the checkbox in the menu again, or use the keyboard shortcut a second time.

XRay mode.gif