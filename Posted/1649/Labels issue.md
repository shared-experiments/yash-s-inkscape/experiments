#### Issue
We have so many labels and still no way right now[^a] for us to track the complete status of reports/issues at a glance. Only open/closed is shown which doesn't tell much. I was thinking if we can step up our label game and make issue tracking more easier.

#### Ideal Label system
For better labelling, I think their categorisation is must and some more status labels should be added (new suggestions [+highlighted+] below). Ideally we should be able to categorise things as:<br>
<br>`type` : bug, feature suggestion, support request, documentation, etc.
<br>`status` : (for example for type:bug) [+needs triage+], need info, [+confirmed+], [+known+], [+WIP+], [+Won't Fix+], [+Archived+] etc.
<br>`area` : UI, UX, Inkscape, performance, [+format compatibility+] etc.
<br>`comment` : [+need help+], [+need investigation+], etc.

(See source[^1] **//** I ain't mentioning OS (already in Inbox) or Imprtance (already in Inkscape).

#### Implementation
To properly implement this kind of label system in GitLab, following are my ideas (in context of current feature-set of GitLab. Plase share if u think there can be some better way of implementation). I have also noted some caveats.

- we use [multiple colon pairs](https://gitlab.com/help/user/project/labels.md#labels-with-multiple-colon-pairs) ; use _type_ as nested & key/scope, and _status_ as its value {as in : `nested::key1::value1`}
<br>Example:
`type::bug::needs triage` , `type::feature request::needs info`
<br>This, when interpreted with open/close of issue, will give complete status info of the issue e.g. **Open>bug>confirmed** , **Closed>bug>Won't Fix** , **Closed(moved)>bug>WIP**
<br> ![image](https://gitlab.com/shared-experiments/yash-s-inkscape/experiments/uploads/d67a09df76e0e6cc3cbe168366fd1eca/image.png)*Illustrative example*
- There is no option to *nest existing labels*. So, by using this method, we wont be able to filter by (say,) `needs info` only, we must choose some other scope like `bug` before.
- There is no way by which we can add *multiple values to same scope*. So we can't implement `area` and `comment` as suggested in ideal, untill something like multi-valued lables (given name by me 😅 i.e. dont try searching for this thing) comes in GitLab!

Suggested Label: ~infrastructure~

#### References
[^a]: <space for comment>
[^1]: https://wiki.blender.org/wiki/Process/Bug_Reports/Triaging_Playbook
