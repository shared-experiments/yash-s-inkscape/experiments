:warning: Lots of title texts used for additional info. (hover over links/images to see those), which may NOT be shown when opened in a mobile client/browser.

#### The old "bug":
I investigated the [given issue](https://inkscape.org/forums/other/copying-shape-from-powerpoint/ "Copying shapes to Inkscape 0.92.4 vs 1.0Alpha"). The reason for above behavior is that in Inkscape 0.92.4, the object was pasted as a whole image, but in new Inkscape (tested 1.0aplha1 onward), it is pasted in the form of its component shapes. So, rather than being a bug, it's a new feature with some issues to resolve/document.

----

#### New issue

On copy-pasting objects from office applications, the source objects break down in a plethora of ways (although not randomly, but specific to source object's property). Separation of fill and stroke into two distinct objects being one of the examples (_the zeroth case in the table_). To study diifferent cases conveniently, I made isolated samples[^1] in Ppt and LibreImpress, and tried copy-pasting[^2] them in Inkscape.

Following are my observations:

**With MS PowerPoint 2007**:
Behaviours list: Addition of ghost object(s), breaking into multiple extra objects, loss of color info, color dithering and so on. ***Observations differ hugely with office 2016 for many of properties, and therefore maybe with other versions too. This table doesn't consider them & contains only 2007 related info.*

<details>
<summary>Click me to collapse/fold detailed and more precise tabular observations.</summary>

| Shape | Color/characteristic | Observations on pasting[^3] | Comment |
| ------ | ------ | ------ | ------ |
| Fill + Line | 0. Solid + Solid | - Fill & Stroke Separated. |  |
| Fill / Line **only**[^1] | 1. Solid without alpha | - Stroke to path (fill) conversion. <br> - Rest okay. | - Sample attached.<br> - W1 applies. |
|  | 2. Solid with Alpha | - Alpha changed to pattern (named like EMFimage0_ref) <br> - 2 solid filled boxes[^4] added. <br> i.e. Broken in 3 parts. | - Sample attached.<br> - W1 for fill, W2 for stroke. |
|  | 3. Gradient without alpha | - No color info at all (probably lost, or incompatible).<br> - 1 extra (clipped/masked) image/ghost object added. <br> i.e. Broken in 2 parts. |   - Exporting as EMF behaves different, <br>similar to original shape, but much more fragmented. <BR> - W3 applies after much finicking. |
|  | 4. Gradient with alpha | - 2 extra (clipped/masked) image objects .... (rest is same as above) <br> i.e. Broken in 3parts. | - Same as above.<br> - Sample attached. |

</details>


**With LibreOffice Impress**:
Behaviours list: Strokes broken into segments & alpha lost. Breaking into extra objects not so common if gradients not involved. Gradient is problematic probably due to the way libreOffice interpolates them.

<details>
<summary>Click me to collapse/fold detailed and more precise tabular observations.</summary>

| Shape | Color/characteristics | Observations[^3] on pasting | Reason/Comment |
| ------ | ------ | ------ | ------ |
| Fill + Line | 0. Solid + Solid | - Fill & Stroke Separated. |  |
| Fill / Line **only**[^1] | 1. Solid without alpha | - Stroke broken into straight segments (which are fill objects & not strokes). |  - Sample attached.<br> - W1 applies. |
|  | 2. Alpha | - Alpha info is lost and set to default value (i.e. 100 which means no transparency). <br> - Same for any available case i.e. for line, fill, as well as gradient too. <br> - Unlike with ppt, no extra objects added. | - W1 for fill, W2 for stroke. |
| Fill only<br><small>∵Not Available for line</small> | 3/4. Gradient | - Broken in individual pieces/shades. | - LibreImpress interpolates gradient in steps. <br> - Sample attached.<br> - W2 applies (even gradient is conserved, though with a small quirk). |

</details>

----

#### Workaround

```mermaid
graph LR;
PPt-->|EMF|LibreDraw-->|export as SVG|Inkscape;
LibreImpress-->|export as SVG|Inkscape;
```
Levels of effectivesness:
W1 i.e. fixed the issue to easily manageable stage.
W2 i.e. simulates the original via masking.
W3 i.e. simulates the original via clipping & dithering.


Keep in mind that it is still a workaround 😅, but I would say it would be mind-blowing workaround if some small quirks here and there can be fixed.

#### Samples:

<details>
<summary>Click me to collapse/fold.</summary>


| Shape types ➜ <br>Source Test Files ↴  | 1. Solid Line | 2. Solid with alpha | 4. Gradient | Other |
| ------ | ------ | ------ | ------ | ------ |
| [MS_Shapes_test.pptx](/uploads/504eeff67542bbcef8dbf2160dbc5f00/2019.11.26_MS_ppt_Shapes_test_file.pptx) | ![Line_Solid_from_MS_ppt.svg](/uploads/9a2b4315eb1f2b9e58de6fa74e1c792e/MS_1_Solid_Line.svg "Line > Solid from MS ppt") | ![Fill_Alpha_from_MS_ppt.svg](/uploads/11a111d2a752273d48ab6845f8c5fbc1/MS_2_Alpha_Fill.svg "Fill > Alpha from MS ppt") | ![Pasted_Gradi.svg](/uploads/c258277ede3f52770f912c687c2eaab4/MS_4_Gradi_alpha_Fill.svg "Fill > Gradient with Alpha *copied* from MS ppt") No color info <br>(∴ invisible). <hr> [Exported_Gradi.emf](/uploads/04f76046511ab6579c28a59728260f00/MS_4_Fill_Gradient_with_Alpha.emf "The exported as EMF version of this type, opposed to copied") | Thumbnail preview for type 4 was shown in the explorer[^5]: ![Thumbnail and EMF file preview](/uploads/26407afa12aaa98158acbff28571bd81/Annotation_2019-11-26_205528.jpg "Thumbnail preview (left) EMF file preview (right)") |
| [Impress_Shapes_test.odp](/uploads/0ddebeb31a49e6cac22244a8d961acbe/2019.11.26_LibreImpress_Shapes_test_file.odp) | ![Line_Solid_from_LibreImpress.svg](/uploads/1bd18cb37fb0d80a2309611d91d61461/LibreImpress_1_Solid_Line.svg "Line > Solid from LibreImpress") | Not Relavant | ![Fill_Gradient_from_LibreImpress.svg](/uploads/23c979a4ab7d2e3b5d2ace73aa003da9/LibreImpress_3_Gradient_Fill.svg "Fill > Gradient from LibreImpress") No of steps for gradient: 4 | |
| EMF to SVG (via LibreDraw) |

</details>

#### What can be done to address this issue?
The suggested workaround can be automated within Inkscape, maybe in the form of an import EMF dialog. This can be done using the code from LibreImpress. This & other relevant info given here should be documented somewhere officially for future users.


#### Software Version and Operating System:

- Inkscape Version: 1.0beta1 (fe3e306, 2019-09-17)
- LibreOffice Version: 6.3.0.4 (x64) Build ID/ See log: [057fc023c990d676a43019934386b85b21a9ee99](https://hub.libreoffice.org/git-core/057fc023c990d676a43019934386b85b21a9ee99)
- Microsoft Office PowerPoint 2007 (12.0.4518.1014) (MSO is same as the number in braces)
- Operating System: Windows 10
- Operating System version: 1903

----
##### P.S.
Feel free to ask for any related info. And also to do your own further research too 😅😋!

#### Footnotes

[^1]: `isolated samples`: as already said, the pasted shape is broken into its stroke and fill. Those are then further broken further depending on their properties. So isolating stroke and fill helps to reduce the number of different cases. Isolation can be done by using either of them `only` i.e. setting the other thing to none in office applications. When other is given some property other than none, then the effects combine. This is also said in footnote 3.
<!--Example: Gradient stroke with alpha fill from *ppt 2007* will have stroke and fill separated in Ink. Those in turn will be carrying the effects of R1 & R3 on stroke (stroke-path, stroke's gradient), R2 on fill (alpha). Basically this means that the result in Inkscape contains all the observations combined from as many conditions apply on it from the table.-->
[^2]: `copy-pasting`: After various testing, it's observed that for simple objects [i.e. without gradients], the behavior of doing it via emf is more or less same as copy-pasting. Mostly differences regarding Clipping/masking are seen. Although major differences regarding breaking is spotted with gradients.
[^3]: [In 1st obs. table] down the table, observations from previous applicable cases also apply unless stated otherwise i.e. only 'new' behavior is given, and old behavior is not repeated again for sake of convenience. 
[^4]: [in the obs. tables] I guess these were bounding boxes.
<!-- [^4]: [in the obs. table] Successfully tried with [irfanview](https://www.irfanview.com/), [XnViewMP](https://www.xnview.com/en/). Also tried with inkview, but noting happened. -->
[^5]: [in the samples table] Thumbnail preview shown using the extension "SVG Explorer Extension dssee from Dotz Software" for windows displays the bounding box with correct gradient.

<!--
**Thank you for filling in a new bug report!**

*More details on how to write a good bug report can be found at https://inkscape.org/contribute/report-bugs/.
Please remember that Inkscape is developed mostly by volunteers in their spare time, and may not be able to respond to reports quickly.*
-->
