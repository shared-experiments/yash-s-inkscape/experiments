```mermaid
graph LR
   subgraph "PowerPoint to Ink"
    a1-->|copy-p/EMF|d2
    a1-->|copy-p|b1
    a1-->|EMF|b2
    b1-->|copy-p|c1
    b1-->|SVG|c1
    b2-->|copy-p|d3
    b2-->|EMF|c3
    b1-->|EMF|c3
    b2-->|SVG|c4
    a1["PowerPoint"]
    b1["Impress"]
    b2["Impress"]
    c1["Not possible 🚫"]
    c3["Ink ❌"]
    c4["Ink 👍"]
    d2["Ink ❌"]
    d3["Raster"]
end
```

```mermaid
graph LR
   subgraph "Impress to Ink"
    a1-->|copy-p/EMF|b1
    a1-->|SVG|b2
    a1["LibreImpress"]
    b1["Ink ❌"]
    b2["Ink 👍"]
end
```

- [ ] ppt - test with (un)grouping
- [ ] ppt - test with gradients
- [ ] imp - ask about CM object (alpha mask)
- [ ] both - test stroke objects

Also, keep in mind that the EMF files made by Impress or Draw do NOT open properly in them, but nicely in Inkscape.
