<!-- 
    See our full bug reporting guidelines at https://inkscape.org/contribute/report-bugs/
    Writing a good bug report will ensure we'll be able to help efficiently. 🙂 
-->

#### Summary:
<!-- Summarize the issue/suggestion concisely: -->

... <write here>

#### Steps to reproduce:
<!-- Describe what you did (step-by-step) so we can reproduce: -->

- open Inkscape
- ... <write here>

#### What happened?

...

#### What should have happened?

...

<!-- 
Example attachments:
Attach the sample file(s) highlighting the issue, if appropriate. 
-->

#### Inkscape Version and Operating System:

```
- Inkscape Version: ... <!-- (run inkscape -V or copy from Help → About Inkscape, top right) -->
- Operating System: ...
- Operating System version: ...
```

<!-- 
    ❤️ Thank you for filling in a new bug report, we appreciate the help! ❤️
    Please be patient while we try to find the time to look into your issue.
    Remember that Inkscape is developed by volunteers in their spare time, we'll try our best to respond to all reports.  
-->

<!-- 
    Please be careful when/after writing #  for example in logs, code, or versions
    - use backticks (`) before and after it, like this - `#1618`
    - use code blocks - triple backticks (```) to format console logs 
    - attach long logs as a text file.
-->